<?php
/**
 * @file
 * project social comments for the project.com
 */

/**
 * Implements hook_menu().
 */
function project_social_comments_menu() {
  $items = array();
  $items['logout/comments'] = array(
    'title' => 'Logout comments',
    'page callback' => 'project_social_comments_logout',
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Callback logout/comments.
 */
function project_social_comments_logout() {
  session_destroy();
  drupal_goto('<front>');
}

/**
 * Implements hook_oauthconnector_fetch_field_value_alter().
 */
function project_social_comments_oauthconnector_fetch_field_value_alter($response, $info, $context) {
  if (!user_is_logged_in() || !project_social_comments_user_login_provider()) {
    $_SESSION['provider'] = $context['provider']->name;
    $_SESSION['project_social_comments'] = $info;

    switch ($context['provider']->name) {
      case 'facebook':
        $url = 'https://graph.facebook.com/'. $info['id'] .'/picture?type=large';
        $name =  'facebook_' . $info['id'];
        break;
      case 'twitter':
        $url = $_SESSION['project_social_comments']['profile_image_url'];
        $name =  'twitter_' . $info['screen_name'];
        break;
      case 'linkedin':
        $url = $_SESSION['project_social_comments']['picture-url'];
        $name = 'linkedin_' . $info['id'];
        break;
      case 'google':
        $url = $_SESSION['project_social_comments']['picture'];
        $name = 'google_' . $info['id'];
        break;
    }
    project_social_comments_external_image($url, $name);
    drupal_goto($_SESSION['oauthconnector_destination']);
  }
}

/**
 * Implements hook_form_alter().
 */
function project_social_comments_form_alter(&$form, &$form_state, $form_id) {
  if (substr($form_id, 0, 12) == 'comment_node' && !user_is_logged_in()) {
    if (!project_social_comments_user_login_provider()) {
      $connect_form = drupal_get_form('connector_button_form');
      if (isset($form['subject'])) $form['subject']['#access'] = FALSE;      
      $form['comment_body']['#access'] = FALSE;
      $form['actions']['#access'] = FALSE;
      $form['author'] = FALSE;
      $destination = array('destination' => drupal_get_path_alias(current_path()));
      $form['comments_authorized']['#markup'] =  '<p>' . t('<a href="@login">Log in</a> or <a href="@register">register</a> to post comments', array('@login' => url('user/login', array('query' => $destination)), '@register' => url('user/register', array('query' => $destination)))) . '</p>';      
      $form['project_comments_login']['#markup'] = drupal_render($connect_form);
    }
    else {
      switch ($_SESSION['provider']) {
        case 'linkedin':
          $real_name = (!empty($_SESSION['project_social_comments']['first-name'])) ? $_SESSION['project_social_comments']['first-name'] : NULL;
          break;
        default:
          $real_name = (!empty($_SESSION['project_social_comments']['name'])) ? $_SESSION['project_social_comments']['name'] : NULL;
    }
      if (!empty($real_name)) {
        $form['author']['name']['#default_value'] = $real_name;
        $form['author']['name']['#access'] = FALSE;
      }

      if ($_SESSION['provider'] == 'twitter') {
        $id = $_SESSION['project_social_comments']['screen_name'];
      }
      else {
        $id = $_SESSION['project_social_comments']['id'];
      }

      $uri =  file_build_uri('project_social_comments/'. $_SESSION['provider'] . '_' . $id . '.jpg');
      $user_picture = theme(
        'image_style',
        array(
          'path' => $uri,
          'style_name' => 'news_author',
        )
      );

      $output_provider =  $user_picture ;
      $output_provider .=  '<p>' . $real_name . '</p>' ;
      $output_provider .= '<p>' . 'authenticated with ' . $_SESSION['provider'] . '</p>';
      $output_provider .= l(t('Logout'), 'logout/comments', array('query' => array('destination' => current_path())));

      $form['provider_name']['#prefix'] = '<div class="provider_info">';
      $form['provider_name']['#suffix'] = '</div>';
      $form['provider_name']['#markup'] = $output_provider;
      $form['provider_name']['#weight'] = -10;
    }
  }

  if ($form_id == 'connector_button_form') {
    if (user_is_logged_in()) {
      $form['#access'] = FALSE;
    }
  }
}

/**
 * Implements hook_connector_action_alter().
 */
function project_social_comments_connector_action_alter(&$connector_actions) {
  if (isset($connector_actions['default']['create account callback'])) {
    unset($connector_actions['default']['create account callback']);
  }
}

/**
 * Implements hook_init().
 */
function project_social_comments_init() {
  drupal_add_css(drupal_get_path('module', 'project_social_comments') . '/css/project_social_comments.css');
}

/**
 * Helper function create file.
 */
function project_social_comments_external_image($url, $name_picture) {
  $uri = 'public://project_social_comments/' . $name_picture .'.jpg';
  if (!file_exists($uri) && !empty($uri)) {
    $dir_path = 'public://project_social_comments';
    if (file_prepare_directory($dir_path, FILE_CREATE_DIRECTORY)) {
      $data = file_get_contents($url);
      $destination = file_create_filename($name_picture . '.jpg', file_default_scheme() . '://project_social_comments/' );
      file_unmanaged_save_data($data, $destination, FILE_EXISTS_REPLACE);
    }
  }
  else {
    return FALSE;
  }
}

/**
 * Implements theme_preprocess_comment().
 */
function project_social_comments_preprocess_comment(&$vars, $hook) {
  if ($vars['elements']['#comment']->uid == 0) {
    $name_picture = $vars['comment']->mail . '.jpg';
    $uri =  file_build_uri('project_social_comments/'. $name_picture);
    $profile_link = project_social_comment_link_profile_img($vars['elements']['#comment']->cid, $uri);
    $vars['picture'] = $profile_link;
  }
}

/**
 * Implements hook_comment_insert().
 */
function project_social_comments_comment_insert($comment) {
  if (user_is_anonymous() || project_social_comments_user_login_provider()) {
    if (isset($_SESSION['provider'])) {
      switch ($_SESSION['provider']) {
        case 'facebook':
          $id = $_SESSION['project_social_comments']['id'];
          break;
        case 'twitter':
          $id = $_SESSION['project_social_comments']['screen_name'];
          break;
        case 'linkedin':
          $id = $_SESSION['project_social_comments']['id'];
          break;
        case 'google':
          $id = $_SESSION['project_social_comments']['id'];
          break;
      }

      $comment->mail = $_SESSION['provider'] . '_' . $id;
      comment_save($comment);
    }
  }
}

/**
 * Preprocesses variables for theme_username().
 *
 * Modules that make any changes to variables like 'name' or 'extra' must insure
 * that the final string is safe to include directly in the output by using
 * check_plain() or filter_xss().
 *
 * @see template_process_username()
 */
function project_social_comments_preprocess_username(&$variables) {
  $node = menu_get_object();
  if (arg(0) == 'node' && $node && isset($node->nid) && is_numeric($node->nid)) {
    $account = $variables['account'];
    $variables['extra'] = '';
    if (empty($account->uid)) {
      $variables['uid'] = 0;
      $cid = isset($variables['account']->cid) ? $variables['account']->cid : null;
      $provider_name = project_social_comment_link_profile($cid, $variables['name'], TRUE);
      $variables['extra'] =  '<small class="authenticated">' . 'authenticated with ' . $provider_name . '</small>';
      $variables['name'] = project_social_comment_link_profile($cid, $variables['name']);
    }
  }
}

/**
 * @param $cid
 * @param $name
 * @param null $provider_name
 * @return string
 */
function project_social_comment_link_profile($cid = NULL, $name, $provider_name = NULL) {
  if (!$cid) {
    return $name;
  }
  
  $info_comment = db_select('comment', 'c')
    ->fields('c', array('mail'))
    ->condition('c.cid', $cid)
    ->execute()
    ->fetchField();

  $provider = explode('_', $info_comment);

  $output = '';

  if (empty($provider_name) && isset($provider[0])) {

    switch ($provider[0]) {
      case 'google':
        $provider[0] = 'plus.google';
        $screen_name = 'http://' . $provider[0] . '.com/' . $provider[1];
        $output = l($name, $screen_name, array(
          'attributes' => array('target' => '_blank')));
        break;
      case 'linkedin':
        $output = l($name, 'http://linkedin.com/profile/view', array(
          'attributes' => array('target' => '_blank'),
          'query' => array('id' => $provider[1]),
        ));
        break;
      case 'facebook':
      case 'twitter':
        $screen_name = 'http://' . $provider[0] . '.com/' . $provider[1];
        $output = l($name, $screen_name, array(
          'attributes' => array('target' => '_blank')));
      case 'devel':
        $output = $name;
        break;
      default:
        $output = $name;
        break;
    }
  }
  else {
   $output = $provider[0];
  }
  return $output;
}

/**
 * @param $cid
 * @param $uri
 * @return string
 */
function project_social_comment_link_profile_img($cid, $uri) {
  $info_comment = db_select('comment', 'c')
    ->fields('c', array('mail'))
    ->condition('c.cid', $cid)
    ->execute()
    ->fetchField();

  // Not an array then bail
  if (!is_array($info_comment)) {
    return;
  }

  $provider = explode('_', $info_comment);
  $output = '';
  $image = theme(
    'image_style',
    array(
      'path' => $uri,
      'style_name' => 'news_author',
    )
  );
    switch ($provider[0]) {
      case 'google':
        $provider[0] = 'plus.google';
        $screen_name = 'http://' . $provider[0] . '.com/' . $provider[1];
        $output = l($image, $screen_name, array(
          'attributes' => array('target' => '_blank'), 'html' => TRUE));
        break;
      case 'linkedin':
        $output = l($image, 'http://linkedin.com/profile/view', array(
          'attributes' => array('target' => '_blank'),
          'query' => array('id' => $provider[1]),
          'html' => TRUE
        ));
        break;
      default:
        $screen_name = 'http://' . $provider[0] . '.com/' . $provider[1];
        $output = l($image, $screen_name, array(
          'attributes' => array('target' => '_blank'), 'html' => TRUE));
    }
  return $output;
}


/**
 * Settings setup provider.
 */
function project_social_comments_provider_settings($keys) {
  foreach($keys as $key => $row) {
    $name = explode('_', $key);
    $provider = oauthconnector_provider_load($name[0]);
    if (!empty($provider)) {
      $provider->consumer_key      = $row['key'];
      $provider->consumer_secret   = $row['secret'];
      oauthconnector_provider_save($provider);
    }
  }
}


function project_social_comments_user_login_provider() {
  return isset($_SESSION['project_social_comments']);
}
